FROM openjdk:10-jre-slim

# Create app directory
RUN mkdir -p /opt/dynmap && mkdir -p /opt/maps

WORKDIR /opt/dynmap

# Bundle app source
COPY . /opt/dynmap

# Volumes
VOLUME /opt/maps

# Expose port 8080
EXPOSE 8080
CMD ["java", "--add-modules", "java.activation", "-jar", "Dynmap-MultiServer.jar"]
